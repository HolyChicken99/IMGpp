//
// Created by alpha on 9/10/21.
//

#ifndef BOOST_FILEVIEWER_MAIN_HPP
#define BOOST_FILEVIEWER_MAIN_HPP

#include <boost/gil.hpp>
#include <boost/gil/extension/io/png.hpp>  // png utilities
#include <boost/gil/extension/numeric/resample.hpp>
#include <boost/gil/extension/numeric/sampler.hpp>
#include <future>
#include <iostream>
#include <stdexcept>  // exception handlers
#include <boost/uuid/uuid.hpp>             // uuid class
#include <boost/uuid/uuid_generators.hpp>  // generators
#include <boost/uuid/uuid_io.hpp>          // streaming operators etc.
#include <filesystem>                      // filesystem operations
#include <fstream>
#include <sstream>
#include<thread>
#include<optional>
#include <string>

// TODO :REMOVE USELESS LIBRARIES AFTER COMPLETION
/*
 * Coding conventions :
 * private member fields being with m_
 * class members use camel case
 * variables inside functions begin with _
 * global constants begin with g_
 * static constants begin with s_
 */

class ImgViewer {
  boost::gil::gray8_image_t img;
  uint16_t m_Width = 0;
  uint16_t m_Height = 0;

 public:
  ImgViewer();

  explicit ImgViewer(
      std::string filename);  /// \constructor default constructor
  void addPath(
      const std::string& filename);  /// \open the image at the specified path
  uint16_t get_Height();  /// \getHeight return the height of the image
  uint16_t get_Width();   /// \getWidth returns the width of the image
  bool saveImg(std::string filename);  /// \saveimage as the filename
  bool saveImg();                      /// \saves Image to /tmp directory

  void flipImgVertical();    /// \flip the image vertically
  void flipImgHorizontal();  /// \flips the image horizontally
  void resizeByPixels(uint16_t height,
                        uint16_t width);  /// \resizes the image
  void gaussianBlur();
};

#endif  // BOOST_FILEVIEWER_MAIN_HPP
