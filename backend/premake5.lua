-- premake5.lua
workspace "boost"
configurations {"Debug", "Release"}
group"bIMG"
project "consoleApp"
    kind "ConsoleApp"
    language "C++"
cppdialect "c++17"
targetdir "bin/%{cfg.buildcfg}"

    includedirs {"/home/alpha/Documents/Headers/boost/","./include/"}
    files { "src/main.cpp"}

    filter "configurations:Debug"
        buildoptions {"-Wall"}
        links{"png","boost_log","pthread"}
        symbols "On"

    filter "configurations:Release" 
        optimize "On"
        buildoptions {"-Wall","-g"}



-- project "test"
    
--     kind "ConsoleApp"
--     language "C++"
--     cppdialect "c++14"
--     targetdir "test/bin"
--     objdir "test/obj"
--     includedirs {"/home/alpha/Documents/BOOST/boost_1_77_0/","./include/"}
--     files {"test/*.cpp"}
--     filter{}
    
    
    
    