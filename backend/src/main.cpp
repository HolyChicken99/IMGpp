
#include <main.hpp>

#ifdef __unix__

ImgViewer::ImgViewer() {
  return;
}

/// TODO : add logging to the function for debug build

ImgViewer::ImgViewer(std::string filename) {
  try {
    boost::gil::read_and_convert_image(filename, img, boost::gil::png_tag());
    this->m_Width = img.width();
    this->m_Height = img.height();
  } catch (std::exception& e) {
    // BOOST_LOG_TRIVIAL(error) << e.what();
  }
}

uint16_t ImgViewer::get_Height() {
  return this->m_Height;
}

uint16_t ImgViewer::get_Width() {
  return this->m_Width;
}

bool ImgViewer::saveImg(const std::string filename) {
  boost::gil::write_view(filename, boost::gil::const_view(img),
                         boost::gil::png_tag());
  return EXIT_SUCCESS;
}

void ImgViewer::flipImgVertical() {
  boost::gil::gray8_pixel_t px;

  for (int i = 0; i < this->m_Height; i++) {
    for (int j = 0; j < this->m_Width / 2; j++) {
      px = *boost::gil::view(this->img).at(j, i);
      boost::gil::view(this->img).at(j, i)[0] =
          boost::gil::view(this->img).at(this->m_Width - j - 1, i)[0];

      boost::gil::view(this->img).at(m_Width - j - 1, i)[0] = px[0];
    }
  }
}

void ImgViewer::flipImgHorizontal() {
  boost::gil::gray8_pixel_t px;
  for (int i = 0; i < this->m_Width; i++) {
    for (int j = 0; j < this->m_Height / 2; j++) {
      px = *boost::gil::view(this->img).at(i, j);
      boost::gil::view(this->img).at(i, j)[0] =
          boost::gil::view(this->img).at(i, m_Height - j - 1)[0];
      boost::gil::view(this->img).at(i, m_Height - j - 1)[0] = px[0];
    }
  }
}

void ImgViewer::resizeByPixels(uint16_t height, uint16_t width) {
  boost::gil::gray8_image_t refImg(width, height);
  boost::gil::resize_view(boost::gil::const_view(img), boost::gil::view(refImg),
                          boost::gil::bilinear_sampler());
  img = refImg;
}

void ImgViewer::addPath(const std::string& filename) {
  try {
    boost::gil::read_and_convert_image(filename, img, boost::gil::png_tag());
    this->m_Width = img.width();
    this->m_Height = img.height();
  } catch (std::exception& e) {
    // BOOST_LOG_TRIVIAL(error) << e.what();
  }
}

bool ImgViewer::saveImg() {
  boost::uuids::uuid uuid = boost::uuids::random_generator()();
  std::stringstream s;
  s << uuid;  // transfer uuid data to string stream object
  std::string filename = s.str();
  filename += ".png";
  std::filesystem::path curr_path = std::filesystem::current_path();
  std::filesystem::create_directory("/tmp/imgpp.tempFile");
  std::filesystem::rename(curr_path / filename, "/tmp/imgpp.files");
}

/// TODO: make a single lamda : pass in the starting and finishing row number ,
/// apply gaussian blur to those  rows
// TODO: make a seperate function taking in a custom kernel value

void ImgViewer::gaussianBlur() {  // default kernel takes a matrix of 9 pixels

  auto blur = [&](uint16_t start,
                  uint16_t end) {  // start and end are the row numbers
    for (int i = start; i <= end; i++) {
      for (int j = 0; j < this->m_Width; j++) {

        auto pixValue = [&](int x, int y) -> std::optional<int> {
          if (x < this->m_Width && x >= 0 && y < this->m_Height && y >= 0) {
            return boost::gil::view(this->img).at(x, y)[0];
          }
          return std::nullopt;
        };
        
        int _sum{0};
        if (pixValue(j + 1, i)) {
            _sum += pixValue(j, i).value();
        }
        if(pixValue(j + 1, i+1)) {
            _sum += pixValue(j + 1, i + 1).value();
        }
        if(pixValue(j + 1, i-1)){
            _sum += pixValue(j + 1, i - 1).value();
        }

        if (pixValue(j - 1, i)) {
            _sum += pixValue(j - 1, i).value();
        }

        if (pixValue(j - 1, i - 1)) {
            _sum += pixValue(j - 1, i - 1).value();
        }
        if(pixValue(j -1,i+1)){
            _sum+=pixValue(j - 1, i + 1).value();
        }
        if(pixValue(j ,i+1)){
            _sum+=pixValue(j,i+1).value();
        }
        if(pixValue(j,i-1)){
            _sum += pixValue(j,i-1).value();
        }
          _sum+=boost::gil::view(this->img).at(j, i)[0];
        boost::gil::view(this->img).at(j,i)[0]=(_sum/9.0);// get the average value of all the kernels
      }
    }
  };

  auto upper_half= std::async(std::launch::async,[&](){
      blur(0,this->m_Height/2);
  });
  auto lower_half= std::async(std::launch::async,[&](){
      blur((this->m_Height/2)+1, this->m_Height-1);

  });


}

#elif defined(_WIN32) || defined(WIN32)

imgViewer::imgViewer() {
  throw std::logic_error("API calls available for U*IX platforms only ");
  return;
}

#endif

#include "temp2.cpp"

//  int main() {

//    imgViewer sol("demo3.png");
//    sol.flip_Img_Horizontal();
//    sol.resize_by_pixels(500,500);
//    //  sol.printInfo();
//    sol.save_Img("lpol1.png");

//    std::string filename("demo3.png");
//    boost::gil::gray8_image_t img;
//  //
//      boost::gil::read_and_convert_image(filename, img,
//      boost::gil::png_tag());
//    boost::gil::gray8_pixel_t px=*boost::gil::const_view(img).at(5,10);
//    // std::cout << "the pixel is "<< (int)px[0] << '\n';
//    boost::gil::rgb8_image_t imgops;
//
//
//    boost::gil::write_view("out-resize.jpg", boost::gil::const_view(imgops),
//    boost::gil::png_tag{});

//  namespace bg = boost::gil;

//  bg::rgb8_image_t img;
//  bg::read_image("test.tiff", img, bg::tiff_tag{});

// test resize_view
// Scale the image to 100x100 pixels using bilinear resampling
//  bg::rgb8_image_t square100x100(100, 100);

//  bg::write_view(img, bg::const_view(square100x100), bg::tiff_tag{});

//    write_view("lol.png",img,png_tag());

//     auto pt = img.dimensions();
//  std::cout << "the images is " << img.height() << " by" << img.I_width() <<
//  '\n';

//    std::string filename("demo.jpeg");
// gil::rgb8_image_t img;
// gil::read_image("demo.jpg",img,boost::gil::jpeg_tag());
// std::cout << "Read complete, got an image " << img.I_width()
//           << " by " << img.height() << " pixels\n";
// gil::rgb8_pixel_t px = *const_view(img).at(5, 10);
// std::cout << "The pixel at 5,10 is "
//           << (int)px[0] << ','
//           << (int)px[1] << ','
//           << (int)px[2] << '\n';
//  }
